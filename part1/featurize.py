import os
import numpy as np
import deepchem as dc
import rdkit

data = np.load("train.npy")
os.chdir("data/")
grid_featurizer = dc.feat.RdkitGridFeaturizer(
    voxel_width=16.0, feature_types=["splif", "ecfp", "hbond"],
    ecfp_power=5, splif_power=5, parallel=True, flatten=True, sanitize=True)

data_featurized = []

for complex in data:
	data_featurized.append(grid_featurizer.featurize(complex))
os.chdir("..")
np.save("train_featurized_2.5.0.npy", data_featurized)
