CMSE 401 Project Part 1
Thanh Lai

~ABSTRACT~

Deepchem is a streamlined machine learning python library for chemists and other scientists. It contains easy to use functions, has a lot of tutorials and benchmarked data, and many ways to featurize molecules. Therefore, this library is the go-to machine learning library for chemists and other scientists who want to incorporate machine learning into their research. Example featurization methods that Deepchem offers is grid featurization, and atomic convolution. It contains a variety of different ML models as well. Deepchem has been used to predict protein-ligand binding energies, properties of molecules, toxicity, etc. and its benchmarked data is especially useful for scientists who are developing their own ML models. This example will use the grid featurization method to featurize protein-ligand complexes. The resultant vectors are numpy vectors and therefore can be used as data in ML models from Deepchem or other modules. Personally, I only use deepchem for its rich molecular featurization methods and I train my models using conventional ML packages with the deepchem-generated feature vectors. However, for those who are interested in using deepchem as their primary library, Deepchem also offers builtin benchmarking datasets and integration with keras. The keras.py example shows how deepchem streamlines benchmarked data and popular ML models. This python example was not benchmarked because it only takes 30 seconds to compute.


BENCHMARK OF MMGBSA/MMPBSA for part 2:

MMGBSA/MMPBSA is a free energy calculation tool used to process molecular dynamics trajectory. It has no relations to Deepchem, but I benchmarked it in preparation for part 2 of the project (where I optimize the submission script using the MPI version). Binding free energy calculations are relevant in research as it can determine the affinity of a drug to a protein. This program was already preinstalled on HPCC. Refer to benchmark.ipynb for the benchmarking of MMGBSA/MMPBSA as well as full detail.


~INSTALLATION OF DEEPCHEM~

Must have anaconda3 installed in HPCC home directory.

1. conda create --name deepchem python=3.7

2. conda activate deepchem

3. pip install tensorflow~=2.4

4. conda install -c conda-forge deepchem

5. conda install -y -c conda-forge rdkit

6. conda install -c omnia pdbfixer

~EXAMPLE CODE~

To do featurization, run

"sbatch featurize.sb"

The feature vectors are exported as "train_featureized_2.5.0.npy"

---

To try a keras example with deepchem, run 

sbatch keras.sb

The train and test R^2 are in the keras.o* file.

~REFERENCES~

1. https://github.com/deepchem/deepchem

2. https://deepchem.readthedocs.io/en/latest/

3. Zhenqin Wu, Bharath Ramsundar, Evan N. Feinberg, Joseph Gomes, Caleb Geniesse, Aneesh S. Pappu, Karl Leswing, Vijay Pande, MoleculeNet: A Benchmark for Molecular Machine Learning, arXiv preprint, arXiv: 1703.00564, 2017.

4. Renxiao Wang, Xueliang Fang, Yipin Lu, Chao-Yie Yang, and Shaomeng Wang. Journal of Medicinal Chemistry 2005 48 (12), 4111-4119. DOI: 10.1021/jm048957q.

5. https://github.com/deepchem/deepchem/blob/master/examples/tutorials/05_Creating_Models_with_TensorFlow_and_PyTorch.ipynb


~SERIAL BENCHMARKS FOR GRID FEATURIZATION AND MMPBSA/MMGBSA~

Check benchmark.ipynb
